from marenapy.paths import *
current_cfg = {}
current_cfg['buildcfg'] = {

  # Name of the buildcfg
  'name': 'default_buildcfg',

  # Compilers
  'backend_compiler':  tool_dir + 'flang-new-default/bin/clang',
  'c_compiler':        tool_dir + 'flang-new-default/bin/clang',
  'cxx_compiler':      tool_dir + 'flang-new-default/bin/clang++',
  'fortran_compiler':  tool_dir + 'flang-new-default/bin/flang -mp',

  # Linkers
  'c_linker':          tool_dir + 'flang-new-default/bin/clang',
  'cxx_linker':        tool_dir + 'flang-new-default/bin/clang++',
  'fortran_linker':    tool_dir + 'flang-new-default/bin/flang -mp',
  'ir_linker':         tool_dir + 'llvm-4.0.1/bin/llvm-link',

  # Flags
  'compiler_flags':    '-march=knl -c -fopenmp -I' + tool_dir + 'flang-new-default/include',
  'ir_compiler_flags': '-march=knl -S -fopenmp -I' + tool_dir + 'flang-new-default/include'
                       '-emit-llvm -Wno-everything',
  'ir_linker_flags':   '-S',
  'linker_flags':      '-L'          + marena_dir + 'orig-libjemalloc/lib -ljemalloc '
                       '-fopenmp -L' + tool_dir   + 'flang-new-default/lib64 -lflang -lflangrti ' # Link flang libraries
                       '-Wl,-rpath,' + tool_dir   + 'flang-new-default/lib64 ' # Add all to the rpath
                       '-Wl,-rpath,' + tool_dir   + 'flang-new-default/lib '
                       '-Wl,-rpath,' + marena_dir + 'orig-libjemalloc/lib',

  'specperllib':       benches_dir + 'cpu2017/bin/modules.specpp',

  # Bools
  'transform': False,
  'toir': False,

  # Tong's tool
  'inputfiles': [],
  'pass_path': marena_dir + 'llparser/passes',
  'sopt': marena_dir + 'llparser/bin/sopt',
}
