from marenapy.cfgs.buildcfgs.marena_test_buildcfg import *

current_cfg['name'] = 'transform_test'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [],
  'env_variables': {},
  'name': 'transform_test_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': [],
  'wrapper': ''
}
