from marenapy.cfgs.buildcfgs.marena_optimized_default_buildcfg import *

current_cfg['name'] = 'marena_knapsack_1.5625'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['hotset.txt'],
  'outputfiles': ['profiling.txt'],
  'peak_rss_cfg': 'default_ir',
  'profcfg': 'marena_shared_site_profiling',
  'knapsack_percentage': 1.5625,
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '4',
    'XPS_NodeMask': '1',
    'XPS_HotAPsFile': 'hotset.txt'},
  'name': 'marena_knapsack_1.5625_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'memreserve-1.5625'],
  'wrapper': 'time -v numactl --preferred=0',
}
