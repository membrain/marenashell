from marenapy.cfgs.buildcfgs.marena_optimized_default_buildcfg import *

current_cfg['name'] = 'marena_avg_ddr_bandwidth_hotset_6.25_6.25'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['hotset.txt'],
  'outputfiles': ['profiling.txt'],
  'peak_rss_cfg': 'default_ir',
  'profcfg': 'marena_ddr_bandwidth_profile',
  'avg_ddr_bandwidth_hotset_percentage': 6.25,
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '4',
    'XPS_NodeMask': '2',
    'XPS_HotAPsFile': 'hotset.txt'},
  'name': 'marena_avg_ddr_bandwidth_hotset_6.25_6.25_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'numastat -m', 'memreserve-6.25'],
  'wrapper': 'time -v numactl --preferred=1',
}

