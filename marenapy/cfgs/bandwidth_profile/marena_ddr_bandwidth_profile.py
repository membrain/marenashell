from marenapy.cfgs.buildcfgs.marena_optimized_default_buildcfg import *

current_cfg['name'] = 'marena_ddr_bandwidth_profile'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt', 'hotset.txt'],
  'outputfiles': ['profiling.txt'],
  'profcfg': 'marena_shared_site_profiling',
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '4',
    'XPS_NodeMask': '2',
    'XPS_HotAPsFile': 'hotset.txt'},
  'name': 'marena_ddr_bandwidth_profile_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'numastat -m'],
  'wrapper': 'time -v numactl --preferred=1',
  'bandwidth_profile': 1
}

