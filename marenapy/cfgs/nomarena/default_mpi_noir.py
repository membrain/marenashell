from marenapy.cfgs.buildcfgs.default_mpi_buildcfg import *

current_cfg['name'] = 'default_mpi_noir'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [],
  'env_variables': {
  },
  'name': 'default_mpi_noir_runcfg',
  'size': 'ref',
  'threads': 4,
  'tools': ['memory', 'numastat -m'],
  'wrapper': 'mpirun -np 64'
}
