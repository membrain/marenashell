from marenapy.cfgs.buildcfgs.marena_buildcfg import *

current_cfg['name'] = 'marena_firsttouch'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt'],
  'env_variables': {
    'XPS_NodeMask': '2'},
  'name': 'marena_firsttouchruncfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory'],
  'wrapper': 'time -v'
}
