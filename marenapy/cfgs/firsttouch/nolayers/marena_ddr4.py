from marenapy.cfgs.buildcfgs.marena_buildcfg import *

current_cfg['name'] = 'marena_ddr4'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt'],
  'env_variables': {
    'XPS_NodeMask': '1'},
  'name': 'marena_ddr4_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory'],
  'wrapper': 'time -v'
}
