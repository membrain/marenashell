from marenapy.cfgs.buildcfgs.marena_buildcfg_onelayer import *

current_cfg['name'] = 'marena_ddr4_onelayer'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt'],
  'env_variables': {
    'XPS_NodeMask': '1'},
  'name': 'marena_ddr4_onelayer_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory'],
  'wrapper': 'time -v'
}
