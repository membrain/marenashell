from marenapy.paths import *
from marenapy.tools import *

from statistics import mean,stdev
from glob import glob

########################################
# Stats
########################################
EXE_TIME     = 'exe_time'
RUNTIME      = 'runtime'
ALLOCS       = 'allocs'
CHUNK_ALLOCS = 'chunk_allocs'
PEAK_RSS     = 'peak_rss'
ACCESSES     = 'accesses'
PEAK_SIZE    = 'peak_size'
NUM_SITES    = 'num_sites'

marena_stats = [
  ALLOCS,
  CHUNK_ALLOCS,
  NUM_SITES
]

########################################
# Benchmarks
########################################

# A dict relating benchmarks to the suite that they're in
bench_suite = {}

def convert_to_seconds(time):
  parts = time.split(':')
  hours = minutes = seconds = 0.0
  if len(parts) > 2:
    hours = float(parts[0])
    parts = parts[1:]

  minutes = float(parts[0])
  seconds = float(parts[1])
  return ( (hours*3600.0) + (minutes*60.0) + seconds )

def convert_to_bytes(kbytes):
  return (float(kbytes) * 1024.0)

def get_peak_rss(bench, cfg_name, results):

  rss = re.compile('\tMaximum resident set size')

  results['default_peak_rss'] = []
  for fname in glob("%s%s/%s/i*/stdout0.txt"% (results_dir, bench, cfg_name)):
    fd = open(fname)
    for line in fd:
      if rss.match(line):
        results['default_peak_rss'].append(convert_to_bytes(line.split()[-1]))
        break
  results['default_peak_rss'] = mean(results['default_peak_rss'])
  print ("peak_rss: %d" % results['default_peak_rss'])

def parse_time(bench, cfg_name, results_dir, results):
  cfg.read_cfg(cfg_name)

  results[RUNTIME] = []
  results[EXE_TIME] = []
  results[PEAK_RSS] = []
  lulesh_fom  = re.compile('FOM')
  pennant_fom = re.compile('hydro cycle')
  amg_fom     = re.compile('Figure of Merit \(FOM_2\):')
  qmcpack_fom = re.compile('  QMC Execution time')
  time = re.compile('\tElapsed \(wall clock\) time')
  rss  = re.compile('\tMaximum resident set size')

  for copy in range(cfg.current_cfg['runcfg']['copies']):
    fd = open('%sstdout%d.txt' % (results_dir, copy), 'r')
    fd.seek(0, 0)
    if bench in ['lulesh']:
      for line in fd:
        if lulesh_fom.match(line):
          results[EXE_TIME].append( ( (1.0/float(line.split()[-2])) * 1000000.0) )
          #results[EXE_TIME].append(float(line.split()[-2]))
        elif rss.match(line):
          results[PEAK_RSS].append(convert_to_bytes(line.split()[-1]))
    elif bench in ['pennant', 'pennant-medium']:
      for line in fd:
        if pennant_fom.match(line):
          results[EXE_TIME].append( ( float(line.split()[-1]) ) )
        elif rss.match(line):
          results[PEAK_RSS].append(convert_to_bytes(line.split()[-1]))
    elif bench in ['amg']:
      for line in fd:
        if amg_fom.match(line):
          results[EXE_TIME].append( (1.0/( float(line.split()[-1])) ) )
          #results[EXE_TIME].append( (float(line.split()[-1]))) 
        elif rss.match(line):
          results[PEAK_RSS].append(convert_to_bytes(line.split()[-1]))
    elif bench in ['qmcpack']:
      for line in fd:
        if qmcpack_fom.match(line):
          results[EXE_TIME].append( ( float(line.split()[-2]) ) )
          #results[EXE_TIME].append( ( 256*16/float(line.split()[-2]) ) )
        elif rss.match(line):
          results[PEAK_RSS].append(convert_to_bytes(line.split()[-1]))
    else:
      for line in fd:
        if time.match(line):
          results[EXE_TIME].append(convert_to_seconds(line.split()[-1]))
        elif rss.match(line):
          results[PEAK_RSS].append(convert_to_bytes(line.split()[-1]))
    fd.seek(0, 0)
    for line in fd:
      if time.match(line):
        results[RUNTIME].append(convert_to_seconds(line.split()[-1]))

  try:
    results[EXE_TIME] = max(results[EXE_TIME])
    results[RUNTIME] = max(results[RUNTIME])
    results[PEAK_RSS] = sum(results[PEAK_RSS])
  except:
    results[EXE_TIME] = 0.0
    results[RUNTIME] = 0.0
    results[PEAK_RSS] = 0.0

# Need to be updated
def parse_marena(bench, cfg_name, results_dir, results):
  cfg.read_cfg(cfg_name)

  #print("Parsing Marena's output.")

  # Output of the destructor
  peak_rss = re.compile('VmHWM:\s+(\d+) kB')
  for copy in range(cfg.current_cfg['runcfg']['copies']):
    fd = open('%sstdout%d.txt' % (results_dir, copy), 'r')
    for line in fd:
      peak_rss_ret = peak_rss.match(line)
      if peak_rss_ret:
        # This value is in kB, convert to bytes
        results[PEAK_RSS] = int(peak_rss_ret.group(1)) * 1024

  # Output of the profiling tools
  new_arena = re.compile('Arena (\d+):')
  peak_rss = re.compile('  Peak RSS: (\d+)')
  arena_accesses = re.compile('  Accesses: (\d+)')
  arena_peak_size = re.compile('  Peak size: (\d+)')
  arena_chunk_allocs = re.compile('  Chunks: (\d+)')
  arena_allocs = re.compile('  Num Allocs: (\d+)')
  aps = re.compile('  Sites: ([\d ]+)')

  for copy in range(cfg.current_cfg['runcfg']['copies']):
    if os.path.isfile('%sprofiling.txt' % (results_dir)):
      fd = open('%sprofiling.txt' % (results_dir), 'r')
      arena = 0
      results['sites'] = {}
      results['arenas'] = {}

      for line in fd:
        # Match the line to the regexes
        new_arena_ret = new_arena.match(line)
        peak_rss_ret = peak_rss.match(line)
        arena_accesses_ret = arena_accesses.match(line)
        arena_peak_size_ret = arena_peak_size.match(line)
        arena_chunk_allocs_ret = arena_chunk_allocs.match(line)
        arena_allocs_ret = arena_allocs.match(line)
        aps_ret = aps.match(line)

        if new_arena_ret:

          arena = int(new_arena_ret.group(1))
          results['arenas'][arena] = {}
          cur_peak_rss = None
          cur_accesses = None
          cur_peak_size = None
          cur_chunk_allocs = None
          cur_allocs = None

        elif aps_ret:

          site = tuple(set(map(int, aps_ret.group(1).split())))
          if not site in results['sites']:
            results['sites'][site] = {}
            results['sites'][site][PEAK_RSS]     = cur_peak_rss
            results['sites'][site][ACCESSES]   = cur_accesses
            results['sites'][site][PEAK_SIZE]  = cur_peak_size
            results['sites'][site][CHUNK_ALLOCS] = cur_chunk_allocs
            results['sites'][site][ALLOCS] = cur_allocs

          else:
            if cur_peak_rss is not None:
              results['sites'][site][PEAK_RSS]   += cur_peak_rss
            if cur_accesses is not None:
              results['sites'][site][ACCESSES] += cur_accesses
            results['sites'][site][PEAK_SIZE]  += cur_peak_size
            results['sites'][site][CHUNK_ALLOCS] += cur_chunk_allocs
            if cur_allocs is not None:
              results['sites'][site][ALLOCS] += cur_allocs

        elif peak_rss_ret:
          cur_peak_rss = int(peak_rss_ret.group(1))
          results['arenas'][arena][PEAK_RSS] = cur_peak_rss
        elif arena_accesses_ret:
          cur_accesses = int(arena_accesses_ret.group(1))
          results['arenas'][arena][ACCESSES] = cur_accesses
        elif arena_peak_size_ret:
          cur_peak_size = int(arena_peak_size_ret.group(1))
          results['arenas'][arena][PEAK_SIZE] = cur_peak_size
        elif arena_chunk_allocs_ret:
          cur_chunk_allocs = int(arena_chunk_allocs_ret.group(1))
          results['arenas'][arena][CHUNK_ALLOCS] = cur_chunk_allocs
        elif arena_allocs_ret:
          cur_allocs = int(arena_allocs_ret.group(1))
          results['arenas'][arena][ALLOCS] = cur_allocs
  
  results[PEAK_RSS] = 0
  if 'sites' in results:
    for site in list(results['sites']):
      if (PEAK_RSS in results['sites'][site]) and (results['sites'][site][PEAK_RSS] is not None):
        results[PEAK_RSS] += results['sites'][site][PEAK_RSS]

  # Aggregate results
  results[ALLOCS] = 0
  results[CHUNK_ALLOCS] = 0
  if 'sites' in results:
    for site in list(results['sites']):
      if results['sites'][site][ALLOCS] != None:
        results[ALLOCS] += results['sites'][site][ALLOCS]
      results[CHUNK_ALLOCS] += results['sites'][site][CHUNK_ALLOCS]

  results[NUM_SITES] = len(results['sites'])

  #print("Finished parsing Marena's output.")

# Import the individual suites
from marenapy.benches.cpu2017 import *
from marenapy.benches.coral import *
from marenapy.benches.xsbench import *

def parse_bench(bench, cfg, results_dir, results, get_profiling=True):
  parse_time(bench, cfg, results_dir, results)
  if get_profiling:
    parse_marena(bench, cfg, results_dir, results)
  func = 'parse_' + bench_suite[bench] + '(\'' + \
         bench + '\',\'' + \
         cfg + '\',\'' + \
         results_dir + '\',' + \
         'results' + '' + ')'
  eval(func)

def get_bench_dir(bench):
  func = 'get_' + bench_suite[bench] + '_dir' + '()'
  return eval(func) + bench + '/'
