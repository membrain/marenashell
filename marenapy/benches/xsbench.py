import os
import pprint
import subprocess
import re
from marenapy.benches.bench import *
import marenapy.cfg as cfg

# Root of the SPEC benches, relative to benchmark directory
xsbench_root = ''

########################################
# All Benches
########################################

for bench in all_benches:
  bench_suite['xsbench'] = 'xsbench'

########################################
# Functions
########################################

def parse_xsbench(bench, cfg_name, path, results):
  cfg.read_cfg(cfg_name)
  fd = open(path + 'stdout0.txt', 'r')

  runtime = re.compile('Runtime:\s+([\d\.]+)\s+seconds')
  for line in fd:
    line = line.rstrip()
    runtime_ret = runtime.match(line)
    if runtime_ret:
      results['performance'] = float(runtime_ret.group(1))

def get_xsbench_dir():
  return benches_dir + xsbench_root
