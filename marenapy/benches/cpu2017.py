import os
import pprint
import subprocess
import re
from marenapy.benches.bench import *
import marenapy.cfg as cfg

# Root of the SPEC benches, relative to benchmark directory
spec_root = 'cpu2017-custom/'

########################################
# Floating Point
########################################

fp_benches = [
  'bwaves',
  'cactubssn',
  'lbm',
  'wrf',
  'cam4',
  'pop2',
  'imagick',
  'nab',
  'fotonik3d',
  'roms'
]

########################################
# Integer
########################################

int_benches = [
  'perlbench',
  'gcc',
  'mcf',
  'omnetpp',
  'xalancbmk',
  'x264',
  'deepsjeng',
  'leela',
  'exchange2',
  'xz'
]


########################################
# By Language
########################################

c_benches = [
  'cactubssn',
  'povray',
  'lbm',
  'wrf',
  'blender',
  'cam4',
  'pop2',
  'imagick',
  'nab',
  'perlbench',
  'gcc',
  'mcf',
  'x264',
  'xz'
]

cxx_benches = [
  'cactubssn',
  'namd',
  'parest',
  'povray',
  'blender',
  'omnetpp',
  'xalancbmk',
  'deepsjeng',
  'leela'
]

fortran_benches = [
  'bwaves',
  'cactubssn',
  'wrf',
  'cam4',
  'pop2',
  'fotonik3d',
  'roms'
  'exchange2',
]

########################################
# All Benches
########################################

all_benches = fp_benches + int_benches

for bench in all_benches:
  bench_suite[bench] = 'cpu2017'

########################################
# Functions
########################################

def parse_cpu2017(bench, cfg_name, results_dir, results):
  cfg.read_cfg(cfg_name)

def get_cpu2017_dir():
  return benches_dir + spec_root
