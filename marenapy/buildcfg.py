import pprint
import os
import stat
import sys
from time import sleep
from shutil import *
import pexpect
import threading
import re
from distutils.dir_util import copy_tree
import marenapy.cfg as cfg
from marenapy.benches.bench import *

cmds = []
threads = {}
statuses = {}
FNULL = open(os.devnull, 'w')

#giant_ir_benches = [ 'qmcpack' ]
giant_ir_benches = [ 'fotonik3d' ]

extra_giant_ir_flags = {
  'qmcpack' : ('-std=c++11 -lstdc++ -lm -Drestrict=__restrict__ ' + \
               '-D__forceinline=__attribute__((always_inline)) -DADD_ -O1 -rdynamic ' + \
               '-Wl,-rpath,${LIBS_DIR}/gcc-7.1.0/lib64:${LIBS_DIR}/hdf5-1.8.20/lib:${LIBS_DIR}/zlib-1.2.11/lib ' + \
               '-L${LIBS_DIR}/gcc-7.1.0/lib64 -L${LIBS_DIR}/lapack-3.8.0/lib64 ' + \
               '-llapack -lblas -L${LIBS_DIR}/lapack-3.8.0/lib64 -lblas  -lxml2 ' + \
               '${LIBS_DIR}/hdf5-1.8.20/lib/libhdf5_debug.so ' + \
               '${LIBS_DIR}/zlib-1.2.11/lib/libz.so -ldl -lm -lz'),
  'fotonik3d' : ( ' ' )
}

def has_ir_files(directory):
  if not os.path.isdir(directory):
    print('The IR directory doesnt exist.')
    return False
  for entity in os.listdir(directory):
    if os.path.isfile(directory + '/' + entity):
      return True
  print('The IR directory has no files.')
  return False

def just_ir_files(adir, filenames):
    retval = []
    for filename in filenames:
      if (not os.path.isdir(os.path.join(adir, filename))) and (not filename.endswith('.o.ll')):
        retval.append(filename)
    return retval

def just_transformed_files(adir, filenames):
    retval = []
    for filename in filenames:
      if (not os.path.isdir(os.path.join(adir, filename))) and (not filename.endswith('.o.ll')):
        retval.append(filename)
    #for x in retval:
    #  print ("  %s/%s" % (adir,x))
    return retval

def run_thread_verbose(cid, build_dir):
  statuses[cid] = subprocess.call(cmds[cid].split(), env=os.environ, cwd=build_dir)

def run_thread(cid, build_dir):
  statuses[cid] = subprocess.call(cmds[cid].split(), env=os.environ, cwd=build_dir, stdout=FNULL, stderr=subprocess.STDOUT)

def run_job(cid, build_dir, verbose=False):
  if verbose:
    t = threading.Thread(target=run_thread_verbose, args=(cid, build_dir,))
  else:
    t = threading.Thread(target=run_thread, args=(cid, build_dir,))
  t.start()
  threads[cid] = t

def run_script(script, build_dir, verbose=False):
  st = os.stat(script)
  os.chmod(script, st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
  proc = pexpect.spawnu(script, timeout=None, logfile=sys.stdout, env=os.environ, cwd=build_dir)
  proc.expect(pexpect.EOF, timeout=None)
  proc.logfile_read = None
  proc.close(force=True)

def write_serial_build(build_dir, full_cmds):
  serial_build_sh = build_dir + "serial_build.sh"
  qbf = open(serial_build_sh,'w')

  print ("#!/bin/bash\nset -x\n", file=qbf)
  for cmds in full_cmds:
    for cmd in cmds:
      print (cmd, file=qbf)
  print ("", file=qbf)
  qbf.close()
  os.chmod(serial_build_sh, os.stat(serial_build_sh).st_mode | stat.S_IEXEC)

def run_parallel_compile(script, build_dir, verbose=False, nthreads=-1):
  global cmds
  global threads
  global statuses
 
  finished = False
  current_running = 0
  step = 0
  full_cmds = []
  full_cmds.append([])

  # Grab all of the compile lines in the file
  #print (script)
  fd = open(script, 'r')
  cnt = 0
  for line in fd:
    line = os.path.expandvars(line.rstrip().lstrip())
    #print (line)
    if nthreads > 0 and cnt == nthreads:
        step += 1
        full_cmds.append([])
        cnt = 0

    if line.endswith('&'):
      line = line[:-1]
      full_cmds[step].append(line)

    elif line != 'wait':
      full_cmds[step].append(line)
      step += 1
      full_cmds.append([])

    elif line == 'wait':
      step += 1
      full_cmds.append([])
      cnt = 0
    cnt += 1

  #print (full_cmds)
  write_serial_build(build_dir, full_cmds)

  total_jobs = sum([len(item) for item in full_cmds])
  completed = 0

  for step_cmds in full_cmds:
    cmds     = step_cmds
    threads  = {}
    statuses = {}
    done     = {}
    running  = []

    for cid in range(len(cmds)):
      run_job(cid, build_dir, verbose=verbose)
      running.append(cid)
      done[cid] = False

    while (len(running) != 0):
      for cid in running:
        if (threads[cid].isAlive() == False) and (not done[cid]):
          if (statuses[cid] != 0):
            run_job(cid, build_dir, verbose=verbose)
          else:
            running.remove(cid)
            done[cid] = True
            completed += 1
            print(("Threads: " + (str(nthreads) if nthreads > 0 else "inf") + " -- " + str(len(running)) + " running (" + str(completed) + "/" + str(total_jobs) + " jobs completed)              "), end='\r', flush=True)
            # print (str(len(running)) + ' ', end='', flush=True),
      sleep(1)

  print ("")

# Builds a benchmark with the given build configuration
def run_buildcfg(bench, cfg_name, skip=[], verbose=False, use_ir_backup=False,
  use_transformed_backup=False, clean=False, nthreads=-1):
  print('Running the build configuration...')
  # Load the configuration from the file
  cfg.read_cfg(cfg_name)

  # Get the source code dir and where we'll store the executable
  build_dir = get_bench_dir(bench) + 'build/'
  exec_dir = get_bench_dir(bench) + 'exe/'
  ir_dir = get_bench_dir(bench) + 'ir/'
  trans_dir = get_bench_dir(bench) + 'transformed/'
  buildcfg_name = cfg.current_cfg['buildcfg']['name']

  # Set all of the environment variables for the scripts
  print('Setting the environment variables...')
  for var in cfg.current_cfg['buildcfg']:
    if (not isinstance(cfg.current_cfg['buildcfg'][var], bool)) and (not isinstance(cfg.current_cfg['buildcfg'][var], list)):
      os.environ[var.upper()] = cfg.current_cfg['buildcfg'][var]
  os.environ['BENCH'] = bench
  os.environ['LIBS_DIR'] = tool_dir

  # Clean up
  if clean:
    print('Cleaning up by running ' + build_dir + 'clean.sh...')
    clean = pexpect.spawnu('bash ' + build_dir + 'clean.sh', timeout=None, cwd=build_dir, logfile=sys.stdout)
    clean.expect(pexpect.EOF, timeout=None)
    ret = clean.wait()
    # Remove all IR files to clean up
    print('Removing all of the generated IR files.')
    ir_file = re.compile('.*\.ll$')
    for f in os.listdir(build_dir):
      if ir_file.match(f):
        os.remove(os.path.join(build_dir, f))
  

  if cfg.current_cfg['buildcfg']['toir']:
    # Open the intermediate scripts
    orig_fd = open(build_dir + 'compile.sh', 'r')
    other = open(build_dir + 'other.sh', 'w')
    ir = open(build_dir + 'compile_to_ir.sh', 'w')
    link_ir = open(build_dir + 'link_ir.sh', 'w')
    trans = open(build_dir + 'transform.sh', 'w')
    compass = open(build_dir + 'compass.sh', 'w')
    obj = open(build_dir + 'compile_to_obj.sh', 'w')
    link = open(build_dir + 'link.sh', 'w')

    # Compile the regexes
    output_file = re.compile('\-o\s+(\S+\.o)\s+')
    comp_line = re.compile('\A\$\{[A-Z]+_COMPILER\}.*\-o\s+(\S+\.o)\s+.*')
    link_line = re.compile('\A\$\{[A-Z]+_LINKER\}.*')
    blank = re.compile('\A\s*\Z')

    # Write the beginning of the giant link line to the file
    link_ir.write('${IR_LINKER} ${IR_LINKER_FLAGS} ')
    #trans.write('#!/bin/bash\n\n')

    # Write the first pass of Compass to its script
    compass.write('#!/bin/bash\n')
    compass.write(os.path.expandvars('${OPT} -load ${COMPASS_PATH}/LLVMCompass.so -compass-mode=analyze -compass-quick-exit -compass ${BENCH}.ll -compass-depth=${CONTEXT_LAYERS} -o /dev/null\nwait\n'))

    run_other = False

    giant_ir_flags      = []
    giant_ir_link_lines = []
    giant_ir_linker     = ""

    for line in orig_fd:
      # If the line invokes the compiler
      res = comp_line.match(line)
      if res:
        # Compile the file to IR instead of object code in "compile_to_ir.sh"
        newline = line.replace('${COMPILER_FLAGS}', '${IR_COMPILER_FLAGS}')
        newline = output_file.sub('-o ' + res.group(1) + '.ll ', newline)
        ir.write(newline)

        # Add this file to be transformed by Compass
        compass.write(os.path.expandvars('${OPT} -load ${COMPASS_PATH}/LLVMCompass.so -O3 -compass-mode=transform -compass ' + res.group(1) + '.ll -compass-depth=${CONTEXT_LAYERS} -o ' + res.group(1) + '.ll &\n'))

        # Add the IR file to be linked together
        link_ir.write(res.group(1) + '.ll ')

        if not bench in giant_ir_benches:
          obj.write( ('${BACKEND_COMPILER} ${COMPILER_FLAGS}' + ' %s.ll -o %s &\n') % (res.group(1), res.group(1)))

        continue
      # If the line invokes the linker
      res = link_line.match(line)
      if res:
        if not bench in giant_ir_benches:
          link.write(line)
        else:
          giant_ir_link_lines += [line]
        continue
      #If the line is blank, avoids needless blank lines in the generated "other.sh" script
      res = blank.match(line)
      if res:
        continue
      other.write(line)
      run_other = True

    if bench in giant_ir_benches:
      #extra_flags = " ".join(giant_ir_flags)
      comp_flags = os.path.expandvars("${COMPILER_FLAGS}").replace('-c','')
      if cfg.current_cfg['buildcfg']['transform']:
        obj.write( ( ('${BACKEND_COMPILER} %s ' % comp_flags) + \
                      '%s.clone.ll -o %s.exe ${LINKER_FLAGS} %s &\n' % (bench, bench, \
                      extra_giant_ir_flags[bench])) )
      else:
        obj.write( ( ('${BACKEND_COMPILER} %s ' % comp_flags) + \
                      '%s.ll -o %s.exe ${LINKER_FLAGS} %s &\n' % (bench, bench, \
                      extra_giant_ir_flags[bench])) )
      #link.write( ('%s ${LINKER_FLAGS} -O3 %s.clone.o -o %s.exe' % (giant_ir_linker,bench,bench)) )
#      for line in giant_ir_link_lines:
#        line = re.sub(r'(\S)*\.o ', ('__my_place__ '), line, 1)
#        line = re.sub(r'(\S)*\.o ', '', line)
#        if cfg.current_cfg['buildcfg']['transform']:
#          line = re.sub('__my_place__', ('%s.clone.o'%bench), line)
#        else:
#          line = re.sub('__my_place__', ('%s.o'%bench), line)
#        link.write(os.path.expandvars(line))

    trans.write('#!/bin/bash\n')
    trans.write(os.path.expandvars('${SOPT} -XX:SkipInst=br,load,store,alloca ' +
                '--ld-pass-path=${PASS_PATH} ' +
                '-call-clone:nlevel=${CONTEXT_LAYERS}:logclone ${BENCH}.ll\n'))
    compass.write('wait\n')
    ## - by tzhou 04/21/2018
    # trans.write(os.path.expandvars('find . -name "*.o.ll" | ' +
    #             'xargs ${SOPT} -XX:SkipInst=br,load,store,alloca ' +
    #             '--ld-pass-path=${PASS_PATH} -include-trace\n'))
    if not bench in giant_ir_benches:
      trans.write(os.path.expandvars('${SOPT} -XX:SkipInst=br,load,store,alloca ' +
                  '--ld-pass-path=${PASS_PATH} -guided-clone\n'))
    link_ir.write(' -o ${BENCH}.ll\n')

       
    # Close all files
    other.close()
    ir.close()
    link_ir.close()
    trans.close()
    compass.close()
    obj.close()
    link.close()
    orig_fd.close()

    # Generate the IR files
    if 1 in skip:
      print('Skipping the generation of IR files...')
    else:
      if use_ir_backup and has_ir_files(ir_dir):
        # Just copy over the untransformed IR files instead of regenerating them
        print('Just copying the untransformed IR files.')
        copy_tree(ir_dir, build_dir)
      else:
        # Run the non-compiler and non-linker lines first
        if run_other:
          print('Running non-matched lines...')
          run_parallel_compile(build_dir + 'other.sh', build_dir, verbose=verbose, nthreads=nthreads)

        # Compile each source file to IR
        print('Compiling to IR...')
        run_parallel_compile(build_dir + 'compile_to_ir.sh', build_dir, verbose=verbose, nthreads=nthreads)
        print('Backing up IR...')
        if os.path.isdir(ir_dir):
          rmtree(ir_dir)
        copytree(build_dir, ir_dir, ignore=just_ir_files)

        # Link all of the IR files into one giant one
        print('Linking the IR together...')
        run_parallel_compile(build_dir + 'link_ir.sh', build_dir, verbose=verbose, nthreads=nthreads)
        print('Backing up the giant IR file...')
        copy(build_dir + bench + '.ll', ir_dir)

    # Transform the IR files
    if cfg.current_cfg['buildcfg']['transform']:
      if 2 in skip:
        print('Skipping the transformation.')
      else:
        if use_transformed_backup and has_ir_files(trans_dir):
          print('Just copying the transformed IR files.')
          copy_tree(trans_dir, build_dir)
        else:
          print('Transforming...')
          run_script(build_dir + 'transform.sh', build_dir, verbose=verbose)
          # Back up the transformed IR
          print('Backing up transformed IR...')
          if os.path.isdir(trans_dir):
            rmtree(trans_dir)
          copytree(build_dir, trans_dir, ignore=just_transformed_files)
          #raise SystemExit(1)
    if cfg.current_cfg['buildcfg']['compass']:
      if 2 in skip:
        print('Skipping the transformation.')
      else:
        if use_transformed_backup and has_ir_files(trans_dir):
          print('Just copying the transformed IR files.')
          copy_tree(trans_dir, build_dir)
        else:
          print('Transforming...')
          run_script(build_dir + 'compass.sh', build_dir, verbose=verbose)
          # Back up the transformed IR
          print('Backing up transformed IR...')
          if os.path.isdir(trans_dir):
            rmtree(trans_dir)
          copytree(build_dir, trans_dir, ignore=just_transformed_files)
          #raise SystemExit(1)

    # Compile each IR file to object code
    if 3 in skip:
      print('Skipping the compilation to object code.')
    else:
      print('Compiling to object code...')
      run_parallel_compile(build_dir + 'compile_to_obj.sh', build_dir, verbose=verbose, nthreads=nthreads)

    # Link the object code into an executable
    if 4 in skip:
      print('Skipping the linking.')
    else:
      print('Linking...')
      run_parallel_compile(build_dir + 'link.sh', build_dir, verbose=verbose, nthreads=nthreads)
  else:
      print('Compiling straight to an executable...')
      run_parallel_compile(build_dir + 'compile.sh', build_dir, verbose=verbose, nthreads=nthreads)

  # Copy the executable for safekeeping
  if not os.path.exists(exec_dir):
    os.makedirs(exec_dir)
  if not os.path.exists(exec_dir + buildcfg_name):
    os.makedirs(exec_dir + buildcfg_name)
  copyfile(build_dir + bench + '.exe', '%s%s/%s.exe' % (exec_dir, buildcfg_name, bench))
  for file in cfg.current_cfg['buildcfg']['inputfiles']:
    copyfile(build_dir + file, '%s%s/%s' % (exec_dir, buildcfg_name, file))

