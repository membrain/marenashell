import os
from shutil import *
from marenapy.paths import *

from marenapy.tools import *
from marenapy.benches.bench import *
from functools import reduce
from math import sqrt
import operator

from statistics import mean,stdev,median

student_t_95 = {
  1  : 12.71,
  2  : 4.303,
  3  : 3.182,
  4  : 2.776,
  5  : 2.571,
  6  : 2.447,
  7  : 2.998,
  8  : 2.306,
  9  : 2.262,
  10 : 2.228,
  11 : 2.201,
  12 : 2.179,
  13 : 2.160,
  14 : 2.145,
  15 : 2.131,
  16 : 2.120,
  17 : 2.110,
  18 : 2.101,
  19 : 2.093,
  20 : 2.086
}

MEAN = "mean"
MEDIAN = "median"

def aggregate_results(bench, cfg_name, iters):
  avgs = dict()
  for i in range(iters):
    for key,val in results[bench][cfg_name][i].items():
      if not isinstance(val, list) and not isinstance(val, dict):
        if key in avgs:
          avgs[key] += val
        else:
          avgs[key] = val
  return avgs

def geo_mean(iterable):
  return (reduce(operator.mul, iterable)) ** (1.0/len(iterable))

def get_results_dir(bench, cfg, i=None):
  if i == None:
    basedir = results_dir + bench + '/' + cfg + '/'
    choices = glob(basedir+"*")
    first_i = min([ int(x.split('/')[-1].strip('i')) for x in choices ])
    return (basedir + 'i' + str(first_i) + '/') 
  else:
    return (results_dir + bench + '/' + cfg + '/i' + str(i) + '/')

def create_results_dir(bench, cfg, i):
  path = get_results_dir(bench, cfg, i)
  print('Deleting %s.' % path)
  rmtree(path, ignore_errors=True)
  if not os.path.exists(path):
    os.makedirs(path)
  return path

# get95CI is used to calculate a confidence interval for the difference between
# two means (basemean and expmean). The value returned can be used to express
# an interval [-ci, +ci], which defines the range that the difference between
# the means will fall between with 95% likelihood.
#
def get95CI(basemean, expmean, basestd, expstd, nruns):

  foo   = ( (basestd**2) / nruns) + ( (expstd**2) / nruns)
  s_x   = sqrt( foo )

  if nruns < 30:

    faz  = (( (basestd**2) / nruns) **2) / (nruns-1)
    fez  = (( (expstd**2)  / nruns) **2) / (nruns-1)
    n_df = int(round( ((foo**2) / (faz + fez)) ))

    ci = (student_t_95[n_df] * s_x)

  else:

    # assume normal distribution
    ci = 1.96 * s_x

  return ci

def expiter(benches, cfgs, iters):

  for bench in benches:
    for cfg in cfgs:
      for i in range(iters):
        yield (bench, cfg, i)

def get_results_dict(benches=[], cfgs=[], iters=1, stat=EXE_TIME, \
  full_parse=False):

  results = dict()
  for bench, cfg_name, i in expiter(benches, cfgs, iters):
    if not bench in results:
      results[bench] = dict()

    if not cfg_name in results[bench]:
      results[bench][cfg_name] = dict()

    results[bench][cfg_name][i] = dict()

    rdict = results[bench][cfg_name][i]
    results_dir = get_results_dir(bench, cfg_name, i)
    if stat in [EXE_TIME,PEAK_RSS,RUNTIME] or full_parse:
      parse_time(bench, cfg_name, results_dir, rdict)
    elif stat in marena_stats or full_parse:
      parse_marena(bench, cfg_name, results_dir, rdict)
    elif stat in numastat_stats or full_parse:
      parse_numastat(results_dir, rdict)
    elif stat in memreserve_stats or full_parse:
      parse_memreserve(results_dir, rdict)
    elif stat in pcm_stats or full_parse:
      parse_pcm(results_dir, rdict)

  return results

def get_report_strs(benches=[], cfgs=[], iters=1, stat=EXE_TIME, \
  basecfg='default_noir', style=MEAN, cis=True, absolute=False):

  if cis and iters < 2:
    cis = False

  results = get_results_dict(benches, cfgs, iters, stat, False)

  report_strs = dict()

  averages = dict()
  for expcfg in cfgs:
    averages[expcfg] = []

  for bench in results.keys():
    report_strs[bench] = dict()

    if not absolute:
      basevals   = [ results[bench][basecfg][i][stat] for i in \
                     results[bench][basecfg].keys() ]
      basemean   = mean(basevals) 
      basemedian = median(basevals) 
      basestd    = stdev(basevals) if len(basevals) > 1 else 0.0

    for expcfg in results[bench].keys():
      expvals   = [ results[bench][expcfg][i][stat] for i in \
                     results[bench][expcfg].keys() ]
      print (expvals)
      expmean   = mean(expvals)
      expmedian = median(expvals)
      expstd    = stdev(expvals) if len(expvals) > 1 else 0.0

      if cis:
        if absolute:
          ci = get95CI(expmean, expmean, expstd, expstd, iters)
        else:
          ci = get95CI(basemean, expmean, basestd, expstd, iters)
          ci /= expmean

      val = None
      if not absolute:
        if style == MEAN:
          val = (float(expmean) / basemean) if basemean > 0.0 else None
        else:
          val = (float(expmedian) / basemedian) if basemedian > 0.0 else None
      else:
        val = (float(expmean)) if style == MEAN else (float(expmedian))

      if val == None:
        report_strs[bench][expcfg] = "N/A" if not cis else ("N/A", "N/A")
      else:
        if absolute:
          report_strs[bench][expcfg] = ("%5.1f" % val) if not cis else \
                                       (("%5.1f" % val), (("%5.1f" % ci)).rjust(6))
        else:
          report_strs[bench][expcfg] = ("%4.3f" % val) if not cis else \
                                       (("%4.3f" % val), (("%3.2f" % ci)).rjust(6))

      averages[expcfg].append(val)

  report_strs["average"] = dict()
  for expcfg in cfgs:
    val = None
    if (len(averages[expcfg]) > 0):
      if not absolute:
        if not (0.0 in averages[expcfg]):
          val = geo_mean(averages[expcfg])
        else:
          val = (geo_mean([(x+1) for x in averages[expcfg]])-1)
      else:
        val = mean(averages[expcfg])

    if val == None:
      report_strs["average"][expcfg] = "N/A" if not cis else ("N/A", "-".rjust(6))
    else:
      if absolute:
        report_strs["average"][expcfg] = ("%5.1f" % val) if not cis else \
                                         (("%5.1f" % val), ("-".rjust(6)))
      else:
        report_strs["average"][expcfg] = ("%4.3f" % val) if not cis else \
                                         (("%4.3f" % val), ("-".rjust(6)))
                                      
  return report_strs

# Gets a dict of results for the ddr_bandwidth configs
def get_ddr_bandwidth_results(benches, cfgs):
  profcfg_results = dict()
  bandwidth_profile_results = dict()
  for bench in benches:
    profcfg_results[bench] = dict()
    bandwidth_profile_results[bench] = dict()
    for cfg_name in cfgs:
      cfg.read_cfg(cfg_name)
      # Get the results for the profcfg
      profcfg_results[bench][cfg_name] = dict()
      bandwidth_profile_results[bench][cfg_name] = dict()

      profcfg_results_dir = get_results_dir(bench, cfg.current_cfg['runcfg']['profcfg'], 0)
      parse_bench(bench, cfg.current_cfg['runcfg']['profcfg'], profcfg_results_dir, profcfg_results[bench][cfg_name])
      bandwidth_profile_dir = results_dir + bench + '/' + cfg_name + '/'

      # Get the bandwidth of the baseline run
      refres = None
      idirs    = [ x+'/' for x in glob(bandwidth_profile_dir+"*") ]
      for x in idirs:
        if x.endswith('i0/'):
          refres = dict()
          parse_pcm(x, refres)
          #print("refres: avg: %5.2f max: %5.2f" % \
          #      (refres[AVG_DDR4_BANDWIDTH], refres[MAX_DDR4_BANDWIDTH]))
      for idir in idirs:
        site = tuple([int(idir.split('/')[-2].strip('i'))])
        bandwidth_profile_results[bench][cfg_name][site[0]] = dict()
        parse_pcm(idir, bandwidth_profile_results[bench][cfg_name][site[0]], refres=refres)

  return (profcfg_results, bandwidth_profile_results)
