import pickle
import os
import stat
import sys
from shutil import *
import pexpect
import threading
import marenapy.cfg as cfg
from marenapy.benches.bench import *

running_benches = {}
output_threads = {}

def capture_output(proc):
  proc.expect(pexpect.EOF, timeout=None)
  
def prepare_runcfg(bench, cfg_name):
  # Load the configuration from the file
  cfg.read_cfg(cfg_name)

  bench_dir = get_bench_dir(bench)
  run_size_dir = bench_dir + 'run-' + cfg.current_cfg['runcfg']['size'] + '/'
  run_dir = bench_dir + 'run/'

  # Set all of the environment variables for the scripts
  for var in cfg.current_cfg['runcfg']['env_variables']:
    print('Setting ' + var.upper())
    os.environ[var.upper()] = str(cfg.current_cfg['runcfg']['env_variables'][var])
  os.environ['BENCH'] = bench
  os.environ['OMP_NUM_THREADS'] = str(cfg.current_cfg['runcfg']['threads'])
  os.environ['KMP_NUM_THREADS'] = str(cfg.current_cfg['runcfg']['threads'])
  os.environ['KMP_AFFINITY'] = 'none'
  os.environ['THREADS'] = str(cfg.current_cfg['runcfg']['threads'])

  # Copy the executable into the run directory for this size
  # Also make it executable
  copyfile(bench_dir + 'exe/' + cfg.current_cfg['buildcfg']['name'] + '/' + bench + '.exe', \
       run_size_dir + bench + '.exe')
  st = os.stat(run_size_dir + bench + '.exe')
  #os.chmod(run_size_dir + bench + '.exe', st.st_mode | stat.S_IXUSR | stat.S_IXGRP)

  # Copy user-supplied files
  for file in cfg.current_cfg['runcfg']['inputfiles']:
    print('Copying ' + file + ' along with the executable.')
    copyfile(bench_dir + 'exe/' + cfg.current_cfg['buildcfg']['name'] + '/' + file, \
         run_size_dir + file)
    if file == 'post-contexts.txt':
      x = open(run_size_dir + file)
      cnt = 0
      for line in x:
        if re.match("(\d)+ ben_", line):
          cnt += 1
      x.close()
      if cnt > 4096:
        os.environ['XPS_EXTRA_SITES'] = 'true'
        print ("%d unique sites! Using extra sites" % cnt)

  # Delete and remake the run directory
  rmtree(run_dir, ignore_errors=True)
  os.makedirs(run_dir)
  
  # Create multiple copies of the run directory
  for i in range(cfg.current_cfg['runcfg']['copies']):
    copy_dir = run_dir + 'copy' + str(i) + '/'
    copytree(run_size_dir, copy_dir)

  # Drop caches
  os.system('echo 3 | sudo tee /proc/sys/vm/drop_caches')

# Runs a benchmark with the given runcfg
def run_runcfg(bench, cfg_name, results_dir):
  cfg.read_cfg(cfg_name)
  bench_dir = get_bench_dir(bench)
  run_size_dir = bench_dir + 'run-' + cfg.current_cfg['runcfg']['size'] + '/'
  run_dir = bench_dir + 'run/'

  # Run all copies
  for i in range(cfg.current_cfg['runcfg']['copies']):
    copy_dir = run_dir + 'copy%d/' % i
    fd = open(results_dir + 'stdout%d.txt' % i, 'wb')
    command = ''
    if cfg.current_cfg['runcfg']['wrapper']:
      command += cfg.current_cfg['runcfg']['wrapper'] + ' '
    command += 'sudo -E bash ' + copy_dir + 'run.sh'
    print('Starting ' + command)
    proc = pexpect.spawn(command, timeout=None, cwd=copy_dir, logfile=fd)
    t = threading.Thread(target=capture_output, args=(proc,))
    t.start()
    running_benches[i] = proc
    output_threads[i] = t
  
  # Wait on them all to finish
  for i in range(cfg.current_cfg['runcfg']['copies']):
    output_threads[i].join()
    running_benches[i].wait()

  # Unset all of the environment variables
  for var in cfg.current_cfg['runcfg']['env_variables']:
    if var.upper() in os.environ:
      del(os.environ[var.upper()])

  # Copy the config for safekeeping
  copyfile(cfg.get_cfg_file(cfg_name), results_dir + 'cfg.py');
  # Copy user-supplied files
  for file in cfg.current_cfg['runcfg']['inputfiles']:
    print('Copying ' + file + ' along with the executable.')
    copyfile(bench_dir + 'exe/' + cfg.current_cfg['buildcfg']['name'] + '/' + file, \
         results_dir + file)

  # Copy files output from this config
  for i in range(cfg.current_cfg['runcfg']['copies']):
    copy_dir = run_dir + 'copy%d/' % i
    for file in cfg.current_cfg['runcfg']['outputfiles']:
      print('Copying ' + file + ' back to the results directory.')
      copyfile(copy_dir + file, results_dir + file)
